import { Prisma } from 'prisma-binding';

export default (url: string) => {
  const prisma = new Prisma({
    typeDefs: 'prisma/generated/prisma.graphql',
    endpoint: url
  });
  return prisma;
};
