import { buildSchema } from 'type-graphql';

import RegistrationResolver from '../resolvers/User/Registration';
import MeResolver from '../resolvers/User/me';

export default async (): Promise<any> => {
    return buildSchema({
        resolvers: [RegistrationResolver, MeResolver],
        emitSchemaFile: true,
    });
};
