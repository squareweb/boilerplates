import 'reflect-metadata';

import { ApolloServer } from 'apollo-server';

import createPrismaInstance from '../prisma/createPrismaInstance';
import buildSchema from './helpers/buildSchema';

const main = async (): Promise<void> => {
    const schema = await buildSchema();

    const Prisma = createPrismaInstance('http://localhost:4466/name/version');

    const server = new ApolloServer({
        schema,
        context: ({ req }): any => ({ req, Prisma }),
        // debug: false // Can disable stacktrace field in error respond or just use NODE_ENV= production mode
    });

    server.listen(4000, () => console.log('Server is listening on port 4000'));
};

main();
