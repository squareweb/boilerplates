import { ObjectType, Field, ID } from 'type-graphql';

@ObjectType()
export default class User {
    @Field(() => ID)
    id: string;

    @Field()
    email: string;

    @Field(() => String, { nullable: true })
    password(): null | string {
        return null;
    }
}
