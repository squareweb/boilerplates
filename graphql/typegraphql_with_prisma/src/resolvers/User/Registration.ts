import { Resolver, Mutation, Arg, Ctx } from 'type-graphql';
import User from '../../models/User';
import RegistrationInput from './registration/RegistrationInput';
import { Prisma as PrismaType } from 'prisma-binding';

@Resolver()
export default class RegistrationResolver {
    @Mutation(() => User)
    async registerUser(
        @Arg('data') { email, password }: RegistrationInput,
        @Ctx() { Prisma }: { Prisma: PrismaType },
    ): Promise<User> {
        const newUser = await Prisma.mutation.createUser({
            data: {
                email,
                password,
            },
        });
        return newUser;
    }
}
