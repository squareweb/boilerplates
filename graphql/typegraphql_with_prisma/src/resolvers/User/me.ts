import { Resolver, Query } from 'type-graphql';

@Resolver()
export default class MeResolver {
    @Query(() => String)
    me(): string {
        return 'works';
    }
}
