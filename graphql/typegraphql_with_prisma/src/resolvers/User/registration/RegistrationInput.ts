import { InputType, Field } from 'type-graphql';
import { IsEmail, Length } from 'class-validator';

@InputType()
export default class RegistrationInput {
    @Field()
    @IsEmail()
    email: string;

    @Field()
    @Length(1, 22)
    password: string;
}
