import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import { getConnection } from 'typeorm';
import User from './models/User';

import v1 from './routes/v1';

const App = express();

App.use(cors());
App.use(bodyParser.json());
App.use(bodyParser.urlencoded({ extended: true }));

App.use('/v1', v1);

App.get('/', async (req: Request, res: Response) => {
    await getConnection()
        .createQueryBuilder()
        .insert()
        .into(User)
        .values({
            name: 'super mario',
        })
        .execute();

    res.status(200).send('Hello World!');
});

export default App;
