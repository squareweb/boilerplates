import { createServer } from 'http';
import App from './app';

import { createConnection } from 'typeorm';

import EntitiesArray from './models';

createConnection({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'root',
    database: 'brend_new_test_case',
    entities: EntitiesArray,
    synchronize: true,
})
    .then(() => console.log('connection created'))
    .catch(err => console.warn('ERROR', err));
const server = createServer(App);

server.listen(3000, () => console.log('Server is listening on port 3000'));
