import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export default class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { length: 128 })
    name: string;

    @Column('varchar', { length: 128 })
    password: string;
}
