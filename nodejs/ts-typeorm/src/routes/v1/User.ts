import express, { Request, Response } from 'express';
import catchError from './catchError';

import validate from './validate';

const User = express.Router();

import * as UserService from '../../services/User';

User.post(
    '/create',
    catchError(async (req: Request, res: Response) => {
        const { name, password } = req.body;

        const token = await UserService.createUser(name, password);

        res.status(200).json({
            token,
        });
    }),
);

User.post(
    '/login',
    validate,
    catchError((req: Request, res: Response) => {
        res.send('OK');
    }),
);

export default User;
