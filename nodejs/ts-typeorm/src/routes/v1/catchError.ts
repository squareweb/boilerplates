import { NextFunction, Request, Response } from 'express';

export default (func: (req: Request, res: Response, next: NextFunction) => void) => {
    return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await func(req, res, next);
        } catch (err) {
            res.status(500).json({
                message: err.message,
                status: 'ERROR',
            });
        }
    };
};
