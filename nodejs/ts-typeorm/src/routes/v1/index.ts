import express from 'express';

import User from './User';

const v1 = express.Router();

v1.use('/user', User);

export default v1;
