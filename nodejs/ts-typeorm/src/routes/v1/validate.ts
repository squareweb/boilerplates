import { validateToken } from '../../services/helpers/auth';
import { Response, NextFunction } from 'express';

export default async (req: any, res: Response, next: NextFunction): Promise<void> => {
    try {
        const { token } = req.body;
        const data = await validateToken(token);
        req.user = data;
        next();
    } catch (err) {
        res.status(500).json({
            type: 'ERROR',
            message: 'Invalid token',
        });
    }
};
