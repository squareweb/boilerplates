import bcrypt from 'bcrypt';

import { getConnection, getRepository } from 'typeorm';
import User from '../models/User';

import { createToken } from './helpers/auth';

export const createUser = async (name: string, password: string): Promise<string> => {
    try {
        const hashedPassword = await bcrypt.hash(password, 10);
        const user = await getRepository(User)
            .createQueryBuilder('user')
            .where('user.name = :name', { name: name })
            .getOne();
        let newUser: any;
        if (!user) {
            newUser = await getConnection()
                .createQueryBuilder()
                .insert()
                .into(User)
                .values({
                    name: name,
                    password: hashedPassword,
                })
                .execute();

            const token = await createToken({ name, id: newUser.id });
            return token;
        } else {
            throw new Error('User already exists');
        }
    } catch (err) {
        throw new Error('Server error');
    }
};

export const loginUser = (): void => {};
