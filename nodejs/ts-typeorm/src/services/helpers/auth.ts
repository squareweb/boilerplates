import jwt from 'jsonwebtoken';

export const createToken = async (dataForToken: any): Promise<string> => {
    try {
        const token = await jwt.sign(
            {
                ...dataForToken,
            },
            'secret',
            { expiresIn: '7d' },
        );
        return token;
    } catch (err) {
        throw new Error('Invalid token');
    }
};

export const validateToken = async (token: string): Promise<string | object> => {
    try {
        const validatedData: string | object = await jwt.verify(token, 'secret');
        return validatedData;
    } catch (err) {
        throw new Error('Invalid token');
    }
};
