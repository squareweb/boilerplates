import Index from './client/pages/index';

export default [
  {
    path: '/',
    ...Index
  }
];
