import axios from 'axios';

export const loadPosts = async () => {
  const response = await axios.get('https://jsonplaceholder.typicode.com/posts');
  const { data } = response;
  return data;
};

export const editPosts = () => {
  return true;
};
