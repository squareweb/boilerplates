import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from '../store/reducers';
import { rootSaga } from '../store/sagas';

export default (defaultState = {}) => {
  const sagaMiddleware = createSagaMiddleware();
  const composeEnh = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const middleware = composeEnh(applyMiddleware(sagaMiddleware));

  const store = createStore(rootReducer, defaultState, middleware);

  sagaMiddleware.run(rootSaga);

  return store;
};
