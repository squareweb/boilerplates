import 'core-js/stable';
import 'regenerator-runtime/runtime';

import React from 'react';
import ReactDOM from 'react-dom';

import { rehydrateMarks } from 'react-imported-component';

import { hot } from 'react-hot-loader/root';

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import { renderRoutes } from 'react-router-config';
import Routes from '../Routes';

import createStore from './createStore.client';

import './sass/index.sass';

const store = createStore(window.__INITIAL_STORE__ ? window.__INITIAL_STORE__ : {});

const app = () => (
  <Provider store={store}>
    <BrowserRouter>{renderRoutes(Routes)}</BrowserRouter>
  </Provider>
);

const AppHot = hot(app);

rehydrateMarks().then(() => {
  ReactDOM.hydrate(<AppHot />, document.querySelector('#root'));
});
