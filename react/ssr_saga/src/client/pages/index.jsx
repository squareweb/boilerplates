import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';

import importedComponent, { ComponentLoader } from 'react-imported-component';

import * as actions from '../../store/actions';

// import Card from '../components/Card';

import dogImage from '../images/dog.jpg';

import { fetchPosts } from '../../store/sagas/posts';

importedComponent(() => import('../components/Card'));

function Index({ loadPosts, posts }) {
  useEffect(() => {
    loadPosts();
  }, []);
  const renderedPosts = posts.map(post => <div key={post.id}>Title: {post.title}</div>);
  const head = () => (
    // Meta Information (header tags)
    <Helmet>
      <title>Home Page</title>
      <meta property="og:title" content="Home Page" />
    </Helmet>
  );

  // AsyncCard.preload();
  return (
    <div>
      {head()}
      <ComponentLoader loadable={() => import('../components/Card')} />
      <img src={dogImage} alt="Dog" className="preview_image" />
      Posts:finally wosrks,s hopefully!!
      {renderedPosts}
    </div>
  );
}

async function loadData(store) {
  store.getState();
  await store.runSaga(fetchPosts).toPromise(); // Call saga function
  // Async code to execute on server side
  // For sagas use: serverSaga
}

const mapDispatchToProps = dispatch => ({
  loadPosts: () => dispatch(actions.loadPosts())
});

const mapStateToProps = state => ({
  posts: state.posts.posts
});

export default {
  component: connect(
    mapStateToProps,
    mapDispatchToProps
  )(Index),
  loadData
};
