import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';

import rootReducer from '../store/reducers';
import { serverSaga } from '../store/sagas';

export default () => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(rootReducer, {}, applyMiddleware(sagaMiddleware));

  store.runServerSaga = () => sagaMiddleware.run(serverSaga);
  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);

  return store;
};
