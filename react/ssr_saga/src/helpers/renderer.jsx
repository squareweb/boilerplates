import React from 'react';
import { renderToString } from 'react-dom/server';
import { Helmet } from 'react-helmet';

import { printDrainHydrateMarks } from 'react-imported-component';

import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';

import serialize from 'serialize-javascript';

import { renderRoutes } from 'react-router-config';
import Routes from '../Routes';

export default (location, store) => {
  const app = (
    <Provider store={store}>
      <StaticRouter context={{}} location={location}>
        {renderRoutes(Routes)}
      </StaticRouter>
    </Provider>
  );

  const content = renderToString(app);
  const initialState = serialize(store.getState());

  const helmet = Helmet.renderStatic();

  const html = `
    <html>
      <head>
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        <link rel="stylesheet" href="public/bundle.css" type="text/css" />
      </head>
      <body>
        <div id="root">${content}</div>
        <script>
          window.__INITIAL_STORE__ = ${initialState}
        </script>
        ${printDrainHydrateMarks()}
        <script src="public/bundle.js"></script>
      </body>
    </html>
  `;

  return html;
};
