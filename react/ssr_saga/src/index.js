import 'core-js/stable';
import 'regenerator-runtime/runtime';

import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { matchRoutes } from 'react-router-config';
import Routes from './Routes';

import renderer from './helpers/renderer';

import createStore from './helpers/createStore.server';

const App = express();

App.use(cors());
App.use(bodyParser());

App.use('/public', express.static('public'));

App.use('/', async (req, res) => {
  const store = createStore();

  const arrayOfLoadDataPromises = matchRoutes(Routes, req.path).map(({ route }) => {
    return route.loadData ? route.loadData(store) : null;
  });

  try {
    const serverSagaPromise = store.runServerSaga().toPromise();
    const loadFunctionPromise = Promise.all(arrayOfLoadDataPromises);
    await Promise.all([serverSagaPromise, loadFunctionPromise]);
    res.status(200).send(renderer(req.path, store));
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

App.listen(3000, () => console.log('Server is listening on port 3000'));
