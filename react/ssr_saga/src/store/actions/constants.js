export const posts = {
  load: {
    start: 'POSTS_LOAD_START',
    success: 'POSTS_LOAD_SUCCESS',
    error: 'POSTS_LOAD_ERROR'
  },
  remove: 'POSTS_REMOVE'
};

export const auth = {};
