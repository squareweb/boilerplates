import * as actionTypes from './constants';

export const loadPosts = () => ({
  type: actionTypes.posts.load.start
});

export const removePosts = () => ({
  type: actionTypes.posts.remove
});
