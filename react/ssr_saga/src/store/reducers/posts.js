import * as actionTypes from '../actions/constants';

const defaultState = {
  posts: [],
  error: {
    status: 'OK',
    message: ''
  }
};

const pushPosts = (state, { payload }) => {
  const { posts } = payload;
  const newArrayOfPosts = [...posts];
  return {
    ...state,
    posts: newArrayOfPosts,
    error: {
      status: 'OK',
      message: ''
    }
  };
};

const removePosts = state => ({
  ...state,
  posts: []
});

const generateTheError = (state, { payload }) => {
  const { error } = payload;
  return {
    ...state,
    error: {
      status: 'ERROR',
      message: error
    }
  };
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.posts.load.success:
      return pushPosts(state, action);
    case actionTypes.posts.load.error:
      return generateTheError(state, action);
    case actionTypes.posts.remove:
      return removePosts(state, action);
    default:
      return state;
  }
};
