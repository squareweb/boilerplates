import { fork, put, all } from 'redux-saga/effects';
import { END } from 'redux-saga';

// Load required sagas
import postsSaga from './posts';
// import postsSaga, { fetchPosts } from './posts';

// Root saga for client application version
export function* rootSaga() {
  yield all([fork(postsSaga)]);
}

// Server saga runner
// Use when you need to start some sagas on each request
export function* serverSaga() {
  // yield all([fork(fetchPosts)]);
  yield put(END);
}
