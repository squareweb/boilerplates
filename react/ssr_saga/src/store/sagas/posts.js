import { take, fork, put, call } from 'redux-saga/effects';

import * as actionTypes from '../actions/constants';
import { loadPosts } from '../../api/postsApi';

// worker saga
export function* fetchPosts() {
  try {
    const posts = yield call(loadPosts);
    yield put({
      type: actionTypes.posts.load.success,
      payload: {
        posts
      }
    });
  } catch (err) {
    yield put({
      type: actionTypes.posts.load.error,
      payload: { error: err }
    });
  }
}

// Watcher saga
export default function* postsSaga() {
  while (true) {
    yield take(actionTypes.posts.load.start);
    yield fork(fetchPosts);
  }
}
