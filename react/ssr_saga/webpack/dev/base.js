const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  context: path.resolve(__dirname, '..', '..'),
  mode: 'development',
  resolve: {
    extensions: ['.jsx', '.js']
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  watch: true,
  watchOptions: {
    poll: 1000
  },
  devtool: 'source-map',
  plugins: [
    new CleanWebpackPlugin()
  ]
};
