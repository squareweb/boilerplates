const webpack = require('webpack');
const path = require('path');

const merge = require('webpack-merge');
const baseConfig = require('./base');

const miniCssExtractPlugin = require('mini-css-extract-plugin');

const axios = require('axios');

const config = {
  entry: [
    'react-hot-loader/patch',
    './src/client/index.jsx'
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '..', '..', 'public'),
    publicPath: '/public/'
  },
  module: {
    rules: [
      {
        test: /\.(sass|scss)$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          publicPath: 'public/'
        },
      }
    ]
  },
  devServer: {
    before: function(app, server, compiler) {
      // require('../../dist/bundle');
      app.get('*', (req, res, next) => {
        if(!/\/public/.test(req.url)) {
          axios.get('http://localhost:3000' + req.url)
          .then(function(response) {
            res.send(response.data);
          })
        } else {
          next()
        }
      })
    },
    publicPath: 'http://localhost:8080/public/'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.PUBLIC_PATH': 'http://localhost:8080/public/'
    }),
    new miniCssExtractPlugin({
      filename: 'bundle.css'
    }),
  ]
};

module.exports = merge(baseConfig, config);
