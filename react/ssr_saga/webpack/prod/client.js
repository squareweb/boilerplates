const path = require('path');

const merge = require('webpack-merge');
const baseConfig = require('./base');

const miniCssExtractPlugin = require('mini-css-extract-plugin');

const config = {
  entry: './src/client/index.jsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '..', '..', 'public'),
    publicPath: '/public/'
  },
  module: {
    rules: [
      {
        test: /\.(sass|scss)$/,
        exclude: /node_modules/,
        use: [miniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          publicPath: 'public/',
        },
      }
    ]
  },
  plugins: [
    new miniCssExtractPlugin({
      filename: 'bundle.css'
    })
  ]
};

module.exports = merge(baseConfig, config);
