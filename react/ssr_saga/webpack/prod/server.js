const path = require('path');

const merge = require('webpack-merge');
const baseConfig = require('./base');

const config = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '..', '..', 'dist')
  },
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          publicPath: 'public/',
          emitFile: false
        },
      }
    ]
  },
  target: 'node'
};

module.exports = merge(baseConfig, config);