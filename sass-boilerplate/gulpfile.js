var gulp = require('gulp');

var notify = require('gulp-notify');

var sass = require('gulp-sass');

var postcss = require('gulp-postcss');
var cssnano = require('cssnano');
var autoprefixer = require('autoprefixer');

var browserSync = require('browser-sync');

function sassTask() {
  return gulp.src('./sass/index.sass')
              .pipe(sass().on("error", notify.onError()))
              .pipe(postcss([cssnano(), autoprefixer()]))
              .pipe(gulp.dest('css'))
              .pipe(browserSync.stream());
}

function watchFiles(done) {
  browserSync.init({
    server: {
      baseDir: './'
    },
    port: 3000,
    notify: false
  });
  browserSync.reload();

  browserSync.watch('index.html').on('change', browserSync.reload);
  browserSync.watch('sass/**/*.sass').on('change', function() {
    setTimeout(() => sassTask(), 500)
  });
  done();
}

gulp.task('default', watchFiles);
